function useConvite() {
  
  const checkEmail = async (email: string) => {
    const response = await fetch('/api/checkEmail', {    
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ email })
    }).then(res => res.json())
    return response
  }

  return {
    checkEmail
  }

}
export default useConvite;