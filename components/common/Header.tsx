import { MdHome, MdCalendarToday, MdChat, MdStar, MdMenu, MdDownload, MdPhoto, MdTextsms, MdVideocam } from 'react-icons/md'
import { Button, Flex, Grid, IconButton, SimpleGrid, Spacer, Text } from "@chakra-ui/react"
import Link from 'next/link'
import Router from 'next/router'

function HeaderComponent(props: any) {
  const { title, icon, actions = false } = props
  const IconButtonComponent = (props: any) => {
    const { icon, onClick, ml } = props
    return(
      <IconButton onClick={onClick} aria-label='menu' ml={ml!=undefined?ml:2} icon={icon} />
    )
  }
  return (
    <>
      <Flex mb={2} mt={2}>
        <IconButtonComponent onClick={()=>Router.push('/login')} ml={0} icon={<MdMenu />} />
        <IconButtonComponent onClick={()=>Router.push('/home')} icon={<MdHome />} />
        <Spacer ml={2} />
        <IconButtonComponent icon={<MdDownload />} />
        <IconButtonComponent onClick={()=>Router.push('/top') } icon={<MdStar />} />
        <IconButtonComponent icon={<MdCalendarToday />} />
        <IconButtonComponent icon={<MdChat />} />
      </Flex>
      <Flex mb={2} bg={'gray.100'} p={2} rounded={'md'}>
        <Text>
          { title }
        </Text>
        <Spacer />
        {icon}
      </Flex>
      {actions &&
      <SimpleGrid columns={3} spacing={2} mb={2}>
        <Button onClick={()=>Router.push('/timeline/mensagem')} leftIcon={<MdTextsms />}>Textos</Button>
        <Button onClick={()=>Router.push('/timeline/foto')} leftIcon={<MdPhoto />} >Foto</Button>
        <Button leftIcon={<MdVideocam />}>Vídeo</Button>
      </SimpleGrid>
      }
    </>
  )
}
export default HeaderComponent
