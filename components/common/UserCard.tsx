import { Box, Text, Button } from "@chakra-ui/react";
import dayjs from "dayjs";

function UserCardComponent(props:any) {
  const { f } = props
  return (
    <>
      <Box shadow={'md'} p={2} roundedTopLeft={'md'} roundedBottomRight={'md'} bg={'gray.100'}>
        <Text fontWeight={'bold'}>{f.following.name}</Text>
        <Text>Cadastro em {dayjs(f.following.createdAt).format('DD/MM/YYYY')}</Text>
        <Text>Segue {f.following._count.following} pessas.</Text>
        <Text>Seguida por {f.following._count.followed} pessoas.</Text>
      </Box>
      <Box bg={'gray.300'} p={2} roundedBottomLeft={'md'} roundedBottomRight={'md'}>
        <Button mr={2}>Deixar de seguir</Button>
        <Button>Ver perfil</Button>
      </Box>
    </>
  );
}
export default UserCardComponent
