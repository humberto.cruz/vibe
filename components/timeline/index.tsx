import { Box, Text, Divider } from "@chakra-ui/react"
import { useRecoilValue } from "recoil"
import useSWR from "swr"
import { tokenAtom } from "../../state/atoms"
import moment from 'moment'
import dayjs from "dayjs"

function TimelineComponent() {
  const token = useRecoilValue(tokenAtom)

  const fetcher = async (url: string) => {    
    const r = await fetch(url, {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
    return await r.json()
  }

  const { data, isValidating, error } = useSWR(token?'/api/timeline':null, fetcher)

  if(!data) return null

  console.log(data)

  return (
    <>
      {data.timeline.map((item:any, index:number) => (
        <Box key={'timeline_'+index}mb={2} bg={'gray.100'} p={2} rounded={'md'}>
          <Text><strong>{item.account.nickname}</strong> postou {dayjs(item.createdAt).format('DD/MM/YYYY [às] HH:mm')}</Text>
          <Divider mt={2} mb={2} />
          <Text>{item.textTimeline.message.title}</Text>
          <Text>{item.textTimeline.message.content}</Text>
        </Box>
        )
      )}
      <Text>Você viu todas as publicações de quem você segue, a seguir outras publicações...</Text>
    </>
  )
}
export default TimelineComponent
