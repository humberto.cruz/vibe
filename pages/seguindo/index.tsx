import { useState } from 'react'
import { Box, Button, Center, Container, Flex, Spacer } from '@chakra-ui/react'
import HeaderComponent from '../../components/common/Header'
import { MdPerson } from 'react-icons/md'
import { parseCookies } from "../../helpers/"
import { prisma } from '../api/_prisma'
import UserCardComponent from '../../components/common/UserCard'


function SeguindoScreen(props:any) {
  const { cookies, following, followed } = props
  const [ followType, setFollowType ] = useState('following')
  return (    
    <Container flex={1}>
      <HeaderComponent title={'Seguidores'} icon={<MdPerson />} />
      <Flex mb={2} p={2} rounded={'md'} bg={'teal.100'}>
        <Button onClick={()=>setFollowType('following')} colorScheme={'teal'} mr={2}>Você Segue</Button>
        <Spacer />
        <Button onClick={()=>setFollowType('followed')} colorScheme={'teal'}>Te Segue</Button>
      </Flex>
      {followType=='following'&&<Box>
        {following.map((f:any, i:number) => {
            return (
              <UserCardComponent key={'foll_'+i} f={f} />
            )
          })}
        </Box>
      }
      {followType=='followed'&&<Box>
        {followed.map((f:any, i:number) => {
            return (
              <UserCardComponent key={'foll_'+i} f={f} />
            )
          })}
        </Box>
      }
    </Container>
  )
}
export default SeguindoScreen

export async function getServerSideProps(context: any) {
  const cookies = await parseCookies(context.req)
  const data = await prisma.user.findUnique({
    where: {
      email: 'user1@gmail.com'
    },
    select: {
      following: {
        select: {
          createdAt: true,
          following: {
            select: {
              id: true,
              createdAt: true,
              name: true,
              email: true,
              _count: {
                select: {
                  following: true,
                  followed: true
                }
              }
            }
          }
        }
      },
      followed: {
        select: {
          createdAt: true,
          following: {
            select: {
              id: true,
              createdAt: true,
              name: true,
              email: true,
              _count: {
                select: {
                  following: true,
                  followed: true
                }
              }
            }
          }
        }
      }
    }
  })
  let jsonData = JSON.parse(JSON.stringify(data))
  return {
    props: {
      cookies,
      following: jsonData?.following,
      followed: jsonData?.followed
    }
  }
}
