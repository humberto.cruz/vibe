import type { NextPage } from 'next'
import { Box, Button, Container } from '@chakra-ui/react'
import HeaderComponent from '../components/common/Header'
import { MdHome } from 'react-icons/md'
import { useRouter } from 'next/router'
import { useRecoilValue } from 'recoil'
import { tokenAtom } from '../state/atoms'
import { useEffect, useState } from 'react'
import { Text } from '@chakra-ui/react'
import TimelineComponent from '../components/timeline'
import { parseCookies } from "../helpers/"

function HomeScreen(props:any) {
  const { cookies } = props
    const router = useRouter()
  
  const [loading, setLoading] = useState(false)

  if (loading) {
    return (
      <Container flex={1}>
        <Text>Carregando...</Text>
      </Container>
    )
  }  
  return (    
    <Container flex={1}>
      <HeaderComponent actions title={'Home'} icon={<MdHome />} />
      <TimelineComponent />
      <Button onClick={()=>router.push('/seguindo')} w={'full'} mr={2}>Seguidores</Button>
    </Container>
  )
}

export default HomeScreen

export async function getServerSideProps(context: any) {
  const cookies = await parseCookies(context.req)
  return {
    props: {
      cookies: cookies && cookies
    }
  }
}
