import { ChevronDownIcon } from "@chakra-ui/icons";
import { Box, Center, Image, Text, Divider, 
  FormControl, FormLabel, Input, Button, FormHelperText, 
  Menu, MenuButton, MenuList, MenuItem, SimpleGrid, useToast } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useState } from "react";
import { useRecoilValue } from "recoil";
import { invitationAtom } from "../../state/atoms";

function Cadastro(props: any) {
  const toast = useToast()
  const route = useRouter()
  const [name, setName] = useState('');
  const [nickname, setNickname] = useState('');
  const [password, setPassword] = useState('');
  const [passConfirm, setPassConfirm] = useState('');
  const [gender, setGender] = useState('MALE');

  const invitation: any = useRecoilValue(invitationAtom);
  const handleSaveUser = async () => {
    if (password!==passConfirm) {
      toast({
        title: 'Falha no cadastro',
        description: 'As senha digitadas são diferentes.',
        status: 'error',
        duration: 5000,
        isClosable: true
      })
      return;
    }
    const user = {
      name,
      password,
      gender,
      email: invitation?.email,
      invitationId: invitation?.id,
      nickname,
      group: invitation?.group
    }
    const result = await fetch(
      '/api/createAccountUser',
      {
        method:'POST',
        body: JSON.stringify(user)
      }
    ).then(res => res.json())
    console.warn(result)
    if (result && result.success) {
      toast({
        title: 'Cadastro realizado com sucesso',
        description: 'Direcionando para o login.',
        status: 'success',
        duration: 5000,
        isClosable: true,
        onCloseComplete: () => {
          props.route.push('/login')
        }
      })
    } else {
      toast({
        title: 'Falha no cadastro',
        description: 'Email já cadastrado.',
        status: 'error',
        duration: 5000,
        isClosable: true
      })
    }
    
  }

  return (
    <Box p={4}>
      <Center flexDirection={'column'}>
        <Image w={200} rounded={'lg'} src={'/images/logo.jpg'} alt='vibe logo' />
        <Text fontSize={48}>VIBE</Text>
      </Center>
      <Divider mt={5} mb={5} />
      <SimpleGrid columns={2} spacing={10} mb={5}>
        <FormControl>
          <FormLabel htmlFor='Apelido'>Apelido</FormLabel>
          <Input value={nickname} onChange={(e)=>setNickname(e.currentTarget.value)} />
        </FormControl>
        <FormControl>
          <FormLabel htmlFor='Tipo'>Tipo</FormLabel>
          <Input readOnly value={invitation&&invitation.group=='SINGLE'?'SINGLE':'COUPLE'} />
        </FormControl>
      </SimpleGrid>
      <FormControl mb={5}>
        <FormLabel htmlFor='genero'>Gênero</FormLabel>
        <Menu>
          <MenuButton as={Button} rightIcon={<ChevronDownIcon />}>
            {gender}
          </MenuButton>
          <MenuList>
            <MenuItem onClick={()=>setGender('MALE')}>Homem</MenuItem>
            <MenuItem onClick={()=>setGender('FEMALE')} >Mulher</MenuItem>
          </MenuList>
        </Menu>
      </FormControl>
      <FormControl mb={5}>
        <FormLabel htmlFor='name'>Nome</FormLabel>
        <Input placeholder='Digite seu nome' id='name' type='text' value={name} 
        onChange={(e)=>setName(e.currentTarget.value)} />
      </FormControl>
      <FormControl mb={5}>
        <FormLabel htmlFor='password'>Senha</FormLabel>
        <Input placeholder='Digite sua senha' id='password' type='password' value={password} 
        onChange={(e)=>setPassword(e.currentTarget.value)} />
      </FormControl>
      <FormControl mb={5}>
        <FormLabel htmlFor='passConfirm'>Confirmar Senha</FormLabel>
        <Input placeholder='Digite sua senha novamente' id='passConfirm' type='password' value={passConfirm} 
        onChange={(e)=>setPassConfirm(e.currentTarget.value)} />
      </FormControl>
      <Button w={'full'} onClick={()=>handleSaveUser()} mt={5}>Salvar</Button>
      <Button w={'full'} onClick={()=>route.push('/')} mt={5}>Cancelar</Button>
    </Box>
  )

}

export default Cadastro;
