import type { NextPage } from 'next'
import { Box, Text, Image,FormControl, 
  FormLabel, Input, FormHelperText, Center, 
  Divider, Button, useToast } from '@chakra-ui/react'
import { useState } from 'react'
import useConvite from '../../hooks/useConvite'
import { useRouter } from 'next/router'
import { useRecoilState } from 'recoil'
import { invitationAtom } from '../../state/atoms'

const Home: NextPage = () => {
  const toast = useToast()
  const route = useRouter()
  const [ email, setEmail ] = useState('')
  const [ isEmailValid, setIsEmailValid ] = useState(undefined)
  const [invitationStore, setInvitationStore] = useRecoilState(invitationAtom)
  const [loading, setLoading] = useState(false)
  const { checkEmail } = useConvite()

  const handleCheckEmail = async () => {
    setLoading(true)
    const invitation = await checkEmail(email)
    if (invitation && !invitation.used) {
      toast({
        title: 'Convite válido',
        description: 'Direcionando para o cadastro.',
        status: 'success',
        duration: 5000,
        isClosable: true,
        onCloseComplete: () => {
          setLoading(false)
          setInvitationStore(invitation)
          // @ts-ignore
          route.push('/convite/cadastro',{ invitation: invitation })
        }
      })
    } else {
      setLoading(false)
      toast({
        title: 'Convite inválido',
        description: 'Não há convite válido para o email informado.',
        status: 'error',
        duration: 5000,
        isClosable: true
      })
    }
  }
  
  return (    
    <Box p={4}>
      <Center flexDirection={'column'}>
        <Image w={200} rounded={'lg'} src={'/images/logo.jpg'} alt='vibe logo' />
        <Text fontSize={48}>VIBE</Text>
      </Center>
      <Divider mt={5} mb={5} />
      <form autoComplete="none">
      <FormControl aria-autocomplete='none' >
        <FormLabel htmlFor='email'>Email</FormLabel>
        <Input autoComplete='off' placeholder='Digite seu email' id='email' type='email' value={email} 
        onChange={(e)=>setEmail(e.currentTarget.value)} />
        {isEmailValid!=undefined&&<FormHelperText color={'red.400'}>Digite um email válido.</FormHelperText>}
      </FormControl>
      <Button w={'full'} isLoading={loading} loadingText={'Validando...'} onClick={()=>handleCheckEmail()} mt={5}>Enviar</Button>
      </form>
    </Box>
  )
}

export default Home
