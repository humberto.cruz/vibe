import type { NextApiRequest, NextApiResponse } from 'next'
import { PrismaClient } from "@prisma/client";
import { v4 } from 'uuid'
import { verify, generate } from 'password-hash'
    
const loginToken = async (req: NextApiRequest, res: NextApiResponse) => {
    /**
     * @description Login by email/password
     * @param email
     * @param password
     */
    const { email, password } = req.body
    const method = req.method

    const prisma = new PrismaClient({
        errorFormat: 'minimal',
    });

    switch(method){
      
      case 'POST':
          try {
            // search user by email
            let user = await prisma.user.findUnique({
                where:{
                    email:email
                }
            })
            // if the user password is 'nova', save a new password using the sended password
            if (user&&user.password=='nova') {
                const hash = generate(password)
                await prisma.user.update({
                    data:{
                        password:hash
                    },
                    where:{
                        id:user?.id
                    }
                })
                user.password = hash
            }
            
            // if the password hash is not the same, return error
            if (!user||!verify(password,user.password)) {
                res.json({
                    success: false,
                    message: 'User not found or invalid password!'
                })
                break
            }
            user.password = ''
            
            // generate a login token
            const token = v4()
            // create a new token for this user
            await prisma.token.create({
                data:{
                    token: token,
                    userId:user.id,
                    valid:true
                }
            })
            // return user data
            res.json({
                success: true,
                token: token,
                user: user
            })
            break
          } catch(err){
              console.log(err)
              res.json({
                  success: false,
                  error:err,
                  message: 'There is a error!'
              })
          }
          break
      default:
          // Se não for o método POST retorna com erro
          res.json({
              success:false,
              error:'Method not implemented!'
          });
          break;
  }  
}

export default loginToken
