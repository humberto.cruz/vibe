import { PrismaClient } from "@prisma/client";
import { v4 } from 'uuid'
import { verify, generate } from 'password-hash'
import { NextApiRequest, NextApiResponse } from "next";
    
const resetPwd = async (req: NextApiRequest, res: NextApiResponse) => {
    /**
     * @description Login by email/password
     * @param email
     * @param password
     * @param passCode
     */
    const body: any = req.body
    const method: any = req.method

    const prisma = new PrismaClient({
        errorFormat: 'minimal',
    });

    switch(method){
      
      case 'POST':
          try {
            // search user by email
            const login: any = await prisma.user.findUnique({
                where:{
                    email:body.email
                },
                include:{
                    tokens:true
                }
            })            
            
            // if the passCode is not the same, return error
            if (!login||body.passCode!=login.passCode) {
                res.json({
                    success: false,
                    message: 'User not found or invalid pass code!'
                })
                break
            }

            // update password
            await prisma.user.update({
                where:{
                    id:login.id
                },
                data:{
                    password: generate(body.password)
                }
            })
            login.password = ''
            login.passCode = ''
            
            // generate a login token
            const token = v4()
            // create a new token for this user
            await prisma.token.create({
                data:{
                    token: token,
                    userId:login.id,
                    valid:true
                }
            })
            // return user data
            res.json({
                success: true,
                message: 'Successful login!',
                token: token,
                user: login
            })
            break
          } catch(err){
              console.log(err)
              //await prisma.$disconnect()
              res.json({
                  success: false,
                  error:err,
                  message: 'There is a error!'
              })
          }
          break
      default:
          // Se não for o método POST retorna com erro
          res.json({
              success:false,
              err:'Method not implemented!'
          });
          break;
  }  
}

export default resetPwd
