// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { prisma } from './_prisma'
import { checkToken } from './_auth'

type Data = {
  error?: string,
  timeline?: any
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  
  const { authorization } = req.headers
  const user: any = await checkToken(authorization||'')
  const followingIds  = await prisma.user.findUnique({
    where: {
      id: user.id
    },
    select: {
      following: {
        select: {
          following:{
            select: {
              id:true
            }
          }
        }
      }
    }
  })
  const timeline = await prisma.timeline.findMany({
    where: {
      userId: {
        in: followingIds?.following.map(f => f.following.id)
      }
    },
    orderBy: {
      createdAt: 'desc'
    },
    include:{
      user: {
        include: {
          account:{
            select:{
              nickname:true
            }
          }
        }
      },
      textTimeline: {
        include: {
          message: true
        }
      },
      imageTimeline: {
        include: {
          image: true
        }
      },
      videoTimeline: {
        include: {
          video: true
        }
      },
      eventTimeline: {
        include: {
          event: true
        }
      }
    }
  })
  res.status(200).json({ timeline })
}