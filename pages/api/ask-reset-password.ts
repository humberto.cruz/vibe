import { prisma } from './_prisma'
import { v4 } from 'uuid'
import nodemailer from 'nodemailer'
import { NextApiRequest, NextApiResponse } from 'next'
    
async function AskResetPassword(req: NextApiRequest, res: NextApiResponse) {
    /**
     * @description Reset password
     * @param email
     */
    const body = req.body
    const method = req.method

    const between = (min:number, max:number) => {  
        return Math.floor(
          Math.random() * (max - min) + min
        )
      }
      

    switch(method){
      
      case 'POST':
          try {
            const  passCode: string = between(100000,999999).toFixed(0)
            const transporter = await nodemailer.createTransport({
                pool: true,
                host: "mail.pricebook.com.br",
                port: 465,
                secure: true, // upgrade later with STARTTLS
                auth: {
                    user: "servicos@pricebook.com.br",
                    //pass: "6#8hKKTid*T@Z2u"
		            pass: "Pricebook2019@"
                },
                tls: {
                    // do not fail on invalid certs
                    rejectUnauthorized: false
                }
            });      
            // verify connection configuration
            let message = {
                from: 'servicos@vibe.com.br', // listed in rfc822 message header
                to: body.email, // listed in rfc822 message header
                subject:'Código para resete de senha',
                text:'Seu código para resete da senha é: '+passCode,
                envelope: {
                    from: 'Vibe <servicos@vibe.com.br>', // used as MAIL FROM: address for SMTP
                    to: body.email // used as RCPT TO: address for SMTP
                }
            }
            await transporter.sendMail(message)
            
            await prisma.user.update({
                where:{
                    email:body.email
                },
                data:{
                    //@ts-ignore
                    passCode: passCode
                }
            })        

            res.status(200).json({
                success: true,
                message: 'Código para recuperação da senha enviado.'
            })
            break
          } catch(err){
              console.log(err)
              res.status(500).json({
                  success: false,
                  error:err,
                  message: 'There is a error!'
              })
          }
          break
      default:
          // Se não for o método POST retorna com erro
          res.status(404).json({
              success:false,
              err:'Method not implemented!'
          });
          break;
  }  
}

export default AskResetPassword
