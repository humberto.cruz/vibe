// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { generate } from 'password-hash'
import { prisma } from './_prisma'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const data = JSON.parse(req.body)
  console.log(data)
  try {
    const hash = generate(data.password)
    const accountUser = await prisma.account.create({
      data: {
        group: data.group,
        //nickname: data.nickname,
        users: {
          create: {
            email: data.email,
            password: hash,
            name: data.name,
          }
        }
      },
      include: {
        users: true
      }
    })
    if (accountUser) {
      await prisma.invitation.update({
        where: {
          id: data.invitationId
        },
        data: {
          used: true,
          invitedUserId: accountUser.users[0].id
        }
      })
      res.status(200).json(accountUser)
    } else {
      res.status(200).json({
        success: false,
        message: 'Erro na criação do usuário.'
      })
    }
  } catch (error) {
    console.log(error)
    res.status(200).json({
      success: false
    })
  }
}
