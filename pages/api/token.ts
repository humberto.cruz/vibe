import { PrismaClient } from "@prisma/client";
import { v4 } from 'uuid'
import { verify, generate } from 'password-hash'
import { NextApiRequest, NextApiResponse } from "next";
    
const Token = async (req: NextApiRequest, res: NextApiResponse) => {
    /**
     * @description Login by token
     * @param token
     */
    const body:any = req.body
    const method:any = req.method

    const prisma = new PrismaClient({
        errorFormat: 'minimal',
    });
    if (!body.token) {
        res.json({
            success: false,
            message: 'There is a error!'
        })
    }
    switch(method){
      
      case 'POST':
          try {
            // search user by token
            const login = await prisma.token.findFirst({
                where:{
                    token:body.token,
                    valid:true
                },
                include:{
                    user:true
                }
            })
            if (login) login.user.password = ''
            // return user data
            res.status(200).json({
                success: login?true:false,
                token: login?login.token:null,
                user: login?login.user:null
            })
            break
          } catch(err){
              console.log(err)
              //await prisma.$disconnect()
              res.json({
                  success: false,
                  error:err,
                  message: 'There is a error!'
              })
          }
          break
      default:
          // Se não for o método POST retorna com erro
          res.json({
              success:false,
              err:'Method not implemented!'
          });
          break;
  }  
}

export default Token
