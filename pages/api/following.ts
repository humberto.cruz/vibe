// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { prisma } from './_prisma'

type Data = {
  error?: string,
  auth?: any
}

export default async function following(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const auth: any = await prisma.user.findFirst({
    include: {
      account: {
        include: {
          users: true
        }
      },
      following: true,
      followed: true
    },
    where: {
      // @ts-ignore
      token: body.token
    }
  })
  if (auth) {
    console.warn(auth)
    res.status(200).json({ auth })
  } else {
    res.status(200).json({
      error: 'Usuário ou senha incorretos'
    })
  }
}