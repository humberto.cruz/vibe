// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { prisma } from './_prisma'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const invitation = await prisma.invitation.findFirst({
    include: {
      user: true,
    },
    where: {
      email: req.body.email
    }
  })
  console.log(invitation)
  if (invitation) {
    res.status(200).json(invitation)
  } else {
    res.status(200).json({
      message: 'No invitation found'
    })
  }
}
