import type { AppProps } from 'next/app'
import { ChakraProvider, extendTheme, ThemeConfig  } from '@chakra-ui/react'
import { RecoilRoot } from 'recoil'
import { CookiesProvider } from "react-cookie"


function MyApp({ Component, pageProps }: AppProps) {
  const config: ThemeConfig = {
    useSystemColorMode: true,
  }
  const theme = extendTheme({ config, shadows: { outline: 'none' } })
  return (
    <CookiesProvider>
      <ChakraProvider theme={theme}>
        <RecoilRoot>
          <Component {...pageProps} />
        </RecoilRoot>
      </ChakraProvider>
    </CookiesProvider>
  )
}

export default MyApp
