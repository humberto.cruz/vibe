import { ChevronDownIcon } from "@chakra-ui/icons";
import { Box, Center, Image, Text, Divider, SimpleGrid, 
  FormLabel, Input, FormControl, Menu, MenuButton, MenuList, MenuItem, Button, useToast } from "@chakra-ui/react";
import { useState } from "react";
import { useRouter } from "next/router";
import { useRecoilState, useRecoilValue } from "recoil";
import { userAtom, tokenAtom } from "../state/atoms";
import { useCookies } from 'react-cookie';

function LoginScreen() {
  const [cookie, setCookie] = useCookies(["user"])
  const [token, setToken] = useCookies(["token"])
  const toast = useToast() 
  const [user, setUser] = useRecoilState(userAtom)
  const [loading, setLoading] = useState(false)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const router = useRouter()
  
  const handleLogin = async () => {    
    setLoading(true)
    const user = {
      email,
      password
    }
    const result = await fetch(
      '/api/login',
      {
        method:'POST',
        body: JSON.stringify(user),
        headers: {
          'Content-Type': 'application/json'
        }
      }
    ).then(res => res.json())
    
    if (result && result.success) {
      setToken('token', JSON.stringify(result.token),{
        path: '/',
        maxAge: 3600,
        sameSite: true
      })
      setCookie('user', JSON.stringify(result.user),{
        path: '/',
        maxAge: 3600,
        sameSite: true
      })

      toast({
        title: 'Login realizado com sucesso',
        description: 'Direcionando para o conteúdo...',
        status: 'success',
        duration: 2000,
        isClosable: true,
        onCloseComplete: () => {
          setLoading(false)
          router.push('/home')
        }
      })
    } else {
      setLoading(false)
      toast({
        title: 'Falha no login',
        description: 'Email ou senha incorretos.',
        status: 'error',
        duration: 5000,
        isClosable: true
      })
    }
  }

  return (
    <Box p={4}>
      <Center flexDirection={'column'}>
        <Image w={200} rounded={'lg'} src={'/images/logo.jpg'} alt='vibe logo' />
        <Text fontSize={48}>VIBE</Text>
      </Center>
      <Divider mt={5} mb={5} />
      <FormControl mb={5}>
        <FormLabel htmlFor='email'>Email</FormLabel>
        <Input placeholder='Digite seu email' id='email' type='text' value={email} 
        onChange={(e)=>setEmail(e.currentTarget.value)} />
      </FormControl>
      <FormControl mb={5}>
        <FormLabel htmlFor='password'>Senha</FormLabel>
        <Input placeholder='Digite sua senha' id='password' type='password' value={password} 
        onChange={(e)=>setPassword(e.currentTarget.value)} />
      </FormControl>
      <Button isLoading={loading} loadingText="Verificando..." width={'full'} onClick={()=>handleLogin()} mt={5}>Login</Button>
    </Box>
  );
}
export default LoginScreen