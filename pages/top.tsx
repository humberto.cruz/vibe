import type { NextPage } from 'next'
import { Container, Heading, Box, Flex, IconButton, Spacer, Text, Grid, Image, Icon, Badge } from '@chakra-ui/react'
import { PhoneIcon, AddIcon, StarIcon, HamburgerIcon, CalendarIcon, ChatIcon, DownloadIcon } from '@chakra-ui/icons'
import HeaderComponent from '../components/common/Header'
import { MdStar } from 'react-icons/md'

const Home: NextPage = () => {
  return (
    <Container flex={1}>
      <HeaderComponent title={'Top 10'} icon={<MdStar />} />
      <Box mb={2}>
        <Grid templateColumns='repeat(3, 1fr)' gap={2}>
          <Box bg={'gray.100'} p={4} rounded={'md'} pos={'relative'}>
            <Image src={'/photos/girl1.jpeg'} alt='girl1' />
            <Badge ml='1' colorScheme='red' size={'lg'} pos={'absolute'} top={5} right={5}>
              1
            </Badge>
            <Text color={'red.500'}>Suzy Sweety</Text>
          </Box>
          <Box bg={'gray.100'} p={4} rounded={'md'} pos={'relative'}>
            <Image src={'/photos/girl7.jpeg'} alt='girl7' />
            <Badge ml='1' colorScheme='red' size={'lg'} pos={'absolute'} top={5} right={5}>
              2
            </Badge>
            <Text color={'red.500'}>Victoria Secret</Text>
          </Box>
          <Box bg={'gray.100'} p={4} rounded={'md'} pos={'relative'}>
            <Image src={'/photos/girl3.jpeg'} alt='girl3' />
            <Badge ml='1' colorScheme='red' size={'lg'} pos={'absolute'} top={5} right={5}>
              3
            </Badge>
            <Text color={'red.500'}>Amanda Blow</Text>
          </Box>
          <Box bg={'gray.100'} p={4} rounded={'md'} pos={'relative'}>
            <Image src={'/photos/girl4.jpeg'} alt='girl4' />
            <Badge ml='1' colorScheme='red' size={'lg'} pos={'absolute'} top={5} right={5}>
              4
            </Badge>
          </Box>
          <Box bg={'gray.100'} p={4} rounded={'md'} pos={'relative'}>
            <Image src={'/photos/girl5.jpeg'} alt='girl5' />
            <Badge ml='1' colorScheme='red' size={'lg'} pos={'absolute'} top={5} right={5}>
              5
            </Badge>
          </Box>
          <Box bg={'gray.100'} p={4} rounded={'md'} pos={'relative'}>
            <Image src={'/photos/girl6.jpeg'} alt='girl6' />
            <Badge ml='1' colorScheme='red' size={'lg'} pos={'absolute'} top={5} right={5}>
              6
            </Badge>
          </Box>
          <Box bg={'gray.100'} p={4} rounded={'md'} pos={'relative'}>
            <Image src={'/photos/girl2.jpeg'} alt='girl2' />
            <Badge ml='1' colorScheme='red' size={'lg'} pos={'absolute'} top={5} right={5}>
              7
            </Badge>
          </Box>
        </Grid>
      </Box>
    </Container>  
  )
}

export default Home
