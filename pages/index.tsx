import type { NextPage } from 'next'
import { Button, Center, Image, Text } from '@chakra-ui/react'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'

const Home: NextPage = () => {
  const [loading, setLoading] = useState(true)
  const router = useRouter()

  useEffect(() => {
    const init = () => {
      const token = localStorage.getItem('token')
      if (token) {
        console.log(token)
        router.replace('/home')
      } else {
        setLoading(false)
      }
    }
    init()
  },[])

  if (loading) {
    return (
      <Center p={4} flexDir={'column'}>
        <Image mb={0} alignSelf={'center'} w={50} rounded={'lg'} src={'/images/logo.jpg'} alt='Vibe' />
        <Text>Carregando...</Text>
      </Center>
    )
  }

  return (    
    <Center p={4} flexDir={'column'}>
      <Image mb={10} alignSelf={'center'} w={200} rounded={'lg'} src={'/images/logo.jpg'} alt='vibe logo' />
      <Link href={'/convite'}>
        <Button mb={10}>Convite</Button>
      </Link>
      <Link href={'/login'}>
        <Button>Login</Button>
      </Link>
    </Center>
  )
}

export default Home
