import type { NextPage } from 'next'
import { Button, Container, FormControl, Input, Textarea } from '@chakra-ui/react'
import HeaderComponent from '../../components/common/Header'
import { MdHome } from 'react-icons/md'
import { useRouter } from 'next/router'
import { useRecoilValue } from 'recoil'
import { tokenAtom } from '../../state/atoms'
import { useEffect, useState } from 'react'
import { Text } from '@chakra-ui/react'
import TimelineComponent from '../../components/timeline'

function NovaMensagemScreen() {
  const token = useRecoilValue(tokenAtom)
  const router = useRouter()
  
  const Router = useRouter()
  const [loading, setLoading] = useState(false)
  const [mensagem, setMensagem] = useState('')

  if (loading) {
    return (
      <Container flex={1}>
        <Text>Carregando...</Text>
      </Container>
    )
  }
  
  return (    
    <Container flex={1}>
      <HeaderComponent title={'Home'} icon={<MdHome />} />
      <FormControl>
        <Text>Texto</Text>
        <Textarea value={mensagem} onChange={(e)=>setMensagem(e.currentTarget.value)} />
      </FormControl>
      <Button mt={2} w={'full'} >Enviar</Button>
      <Button onClick={()=>Router.back()} mt={2} w={'full'} >Cancelar</Button>
    </Container>
  )
}

export default NovaMensagemScreen
