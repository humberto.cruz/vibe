import { Box, Button, Container, FormControl, Input, Textarea } from '@chakra-ui/react'
import HeaderComponent from '../../components/common/Header'
import { MdHome } from 'react-icons/md'
import { useRouter } from 'next/router'
import { useRecoilValue } from 'recoil'
import { tokenAtom } from '../../state/atoms'
import { useEffect, useState, useRef } from 'react'
import { Text } from '@chakra-ui/react'
import TimelineComponent from '../../components/timeline'

function NovaFotoScreen() {
  const token = useRecoilValue(tokenAtom)
  const router = useRouter()
  const inputRef = useRef<HTMLInputElement>(null)
  
  const Router = useRouter()
  const [loading, setLoading] = useState(false)
  const [mensagem, setMensagem] = useState('')

  const openPhotos = () => {
    inputRef.current.click()
  }
  
  const handlePhoto = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.currentTarget.files?.[0]
    console.log(file)
    
    if (file) {
      const formData = new FormData()
      formData.append('file', file)
      //setLoading(true)
      fetch('/api/photo', {
        method: 'POST',
        body: formData
      })
      .then(res => res.json())
      .then(res => {
        //setLoading(false)
        if (res.error) {
          setMensagem(res.error)
        } else {
          setMensagem('')
          //Router.push('/timeline')
        }
      }
      )
    }
    
  }
  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.value = ''
    }
  }
  , [])

  if (loading) {
    return (
      <Container flex={1}>
        <Text>Carregando...</Text>
      </Container>
    )
  }
  
  return (    
    <Container flex={1}>
      <HeaderComponent title={'Home'} icon={<MdHome />} />
      <FormControl>
        <Text>Foto</Text>
        <Button onClick={openPhotos} w={'full'}>Tirar foto</Button>
        
          <Input ref={inputRef} type='file' onChange={handlePhoto} />
        
      </FormControl>
      <Button mt={2} w={'full'} >Enviar</Button>
      <Button onClick={()=>Router.back()} mt={2} w={'full'} >Cancelar</Button>
    </Container>
  )
}

export default NovaFotoScreen
