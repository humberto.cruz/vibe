import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html>
      <style global jsx>{`
        .bgcolor-lightgray {
          background-color: #EFEFEF;
        }`}</style>
      <Head />
      <body className='bgcolor-lightgray'>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
