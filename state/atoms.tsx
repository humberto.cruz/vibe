import {
    atom,
    selector
} from 'recoil';

const tokenAtom = atom({
    key:'token',
    default: undefined
})
const invitationAtom = atom({
    key:'invitation',
    default: undefined
})
const userAtom = atom({
    key:'user',
    default: undefined
})

    
export { tokenAtom, invitationAtom, userAtom };
