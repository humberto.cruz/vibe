/*
  Warnings:

  - You are about to drop the column `ownerId` on the `Timeline` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE `Timeline` DROP FOREIGN KEY `Timeline_ownerId_fkey`;

-- AlterTable
ALTER TABLE `Account` ADD COLUMN `nickname` VARCHAR(191) NULL;

-- AlterTable
ALTER TABLE `Timeline` DROP COLUMN `ownerId`;
